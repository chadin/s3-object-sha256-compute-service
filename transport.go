package main

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/go-kit/kit/endpoint"
)

//transport.go is where the transport layer resides

//Define GRPC request and response
type computeRequest struct {
	Bucket string
	Key    string
}

type computeResponse struct {
	Hash  string
	Error string `json:",omitempty"`
}

//Convert each of the service's methods to an Endpoint
func makeComputeEndpoint(svc S3ObjectSha256ComputeService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(computeRequest)
		hash, err := svc.Compute(req.Bucket, req.Key)
		if err != nil {
			return computeResponse{"", err.Error()}, nil
		}
		return computeResponse{hash, ""}, nil
	}
}

func decodeComputeRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request computeRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}
