package main

import (
	"net/http"
	"os"
	"time"

	"github.com/go-kit/kit/log"

	httptransport "github.com/go-kit/kit/transport/http"
)

//https://gokit.io/examples/stringsvc.html

func main() {
	logger := log.NewLogfmtLogger(os.Stderr)

	var svc S3ObjectSha256ComputeService
	svc = s3ObjectSha256ComputeService{}
	svc = loggingMiddleware{logger, svc}

	computeHander := httptransport.NewServer(
		makeComputeEndpoint(svc),
		decodeComputeRequest,
		encodeResponse,
	)

	http.Handle("/compute", computeHander)

	logger.Log(
		"service", "up",
		"port", 8080,
	)

	http.ListenAndServe(":8080", nil)
}

type loggingMiddleware struct {
	logger log.Logger
	next   S3ObjectSha256ComputeService
}

func (mw loggingMiddleware) Compute(bucket, key string) (hash string, err error) {
	defer func(begin time.Time) {
		mw.logger.Log(
			"method", "compute",
			"bucket", bucket,
			"key", key,
			"hash", hash,
			"err", err,
			"took", time.Since(begin),
		)
	}(time.Now())

	hash, err = mw.next.Compute(bucket, key)
	return
}
