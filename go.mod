module chadin/s3-object-sha256-compute-service

go 1.14

require (
	github.com/aws/aws-sdk-go v1.34.10
	github.com/go-kit/kit v0.10.0
	github.com/joho/godotenv v1.3.0
	github.com/smartystreets/goconvey v1.6.4
)
