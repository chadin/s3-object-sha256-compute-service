package main

import (
	"crypto/sha256"
	"fmt"
	"io"
	"os"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

//service.go is where business logic resides

//Start with an interface
type S3ObjectSha256ComputeService interface {
	Compute(string, string) (string, error)
}

//Implement the business logic
type s3ObjectSha256ComputeService struct{}

func (s3ObjectSha256ComputeService) Compute(bucket, key string) (string, error) {
	//Download file
	var err error

	localFileName := "/tmp/downloaded-file"
	startTime := time.Now()
	//log.Info("start download")
	err = download(bucket, key, localFileName)
	endTime := time.Now()
	/*log.
	WithField("time_taken", endTime.Sub(startTime)).
	WithField("time_taken_string", endTime.Sub(startTime).String()).
	Info("finish download")*/
	if err != nil {
		return "", err
	}
	defer os.Remove(localFileName)

	//Open and compute
	startTime = time.Now()
	//log.Info("start compute sha256")
	hash, err := computeSHA256(localFileName)
	endTime = time.Now()
	/*log.
	WithField("time_taken", endTime.Sub(startTime)).
	WithField("time_taken_string", endTime.Sub(startTime).String()).
	Info("finish compute sha256")*/
	_ = startTime
	_ = endTime

	return hash, err
}

func computeSHA256(localFileName string) (string, error) {
	f, err := os.Open(localFileName)
	if err != nil {
		return "", err
	}
	defer f.Close()

	h := sha256.New()
	if _, err := io.Copy(h, f); err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", h.Sum(nil)), nil
}

func download(bucket, key, localFileName string) error {
	//https://docs.aws.amazon.com/sdk-for-go/api/service/s3/

	// The session the S3 Downloader will use
	sess := session.Must(session.NewSession())

	// Create a downloader with the session and default options
	downloader := s3manager.NewDownloader(sess)

	// Create a file to write the S3 Object contents to.
	f, err := os.Create(localFileName)
	if err != nil {
		return fmt.Errorf("failed to create file %q, %v", localFileName, err)
	}

	// Write the contents of S3 Object to the file
	n, err := downloader.Download(f, &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return fmt.Errorf("failed to download file, %v", err)
	}
	/*log.
	WithField("file_size", n).
	Infof("file downloaded, %d bytes\n", n)*/
	_ = n
	return nil
}
