package main

import (
	"fmt"
	"os"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/joho/godotenv"
	. "github.com/smartystreets/goconvey/convey"
)

func TestComputeSHA256(t *testing.T) {
	Convey("given a valid local file", t, func() {
		localFileName := "test.txt"
		expectedHash := "2d711642b726b04401627ca9fbac32f5c8530fb1903cc4db02258717921a4881"

		Convey("compute sha256", func() {
			hash, err := computeSHA256(localFileName)
			Convey("should be successful", func() {
				So(err, ShouldBeNil)
				Convey("hash should match expected hash", func() {
					So(hash, ShouldEqual, expectedHash)
				})
			})
		})
	})
}

func TestCompute(t *testing.T) {
	var err error
	err = godotenv.Load(".testenv")
	if err != nil {
		t.Fatal(err)
	}

	svc := s3ObjectSha256ComputeService{}

	//integration test
	SkipConvey("given a valid s3 file", t, func() {
		localFileName := "test.txt"
		expectedHash := "2d711642b726b04401627ca9fbac32f5c8530fb1903cc4db02258717921a4881"

		bucket := "chadin-sg"
		key := "s3-file-sha256-computer-test-1.txt"

		_ = upload(bucket, key, localFileName)

		Convey("compute s3 sha256", func() {
			hash, err := svc.Compute(bucket, key)
			Convey("should be successful", func() {
				So(err, ShouldBeNil)
				Convey("hash should match expected hash", func() {
					So(hash, ShouldEqual, expectedHash)
				})
			})
		})
	})

	//integration test
	SkipConvey("given an invalid s3 file", t, func() {
		bucket := "chadin-sg"
		key := "somefilethatdoesntexist"

		Convey("compute s3 sha256", func() {
			_, err := svc.Compute(bucket, key)
			Convey("should fail", func() {
				So(err, ShouldNotBeNil)

				//log.Info(err)
			})
		})
	})

	//integration test
	SkipConvey("given an invalid bucket", t, func() {
		bucket := "someinvalidbucket"
		key := "somefilethatdoesntexist"

		Convey("compute s3 sha256", func() {
			_, err := svc.Compute(bucket, key)
			Convey("should fail", func() {
				So(err, ShouldNotBeNil)

				//log.Info(err)
			})
		})
	})
}

func upload(bucket, key, localFileName string) error {
	// The session the S3 Uploader will use
	sess := session.Must(session.NewSession())

	// Create an uploader with the session and default options
	uploader := s3manager.NewUploader(sess)

	f, err := os.Open(localFileName)
	if err != nil {
		return fmt.Errorf("failed to open file %q, %v", localFileName, err)
	}

	// Upload the file to S3.
	result, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
		Body:   f,
	})
	if err != nil {
		return fmt.Errorf("failed to upload file, %v", err)
	}
	//log.Infof("file uploaded to, %s\n", aws.StringValue(&result.Location))
	_ = result
	return nil
}
